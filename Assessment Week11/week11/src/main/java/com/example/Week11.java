package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week11 {

	public static void main(String[] args) {
		SpringApplication.run(Week11.class, args);
	}

}
