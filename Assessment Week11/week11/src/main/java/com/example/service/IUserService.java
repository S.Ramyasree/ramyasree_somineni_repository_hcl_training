package com.example.service;

import java.util.List;

import com.example.beans.User;
import com.example.exception.ProjectException;

public interface IUserService {

	public User addUser(User user);

	public List<User> getAllUsers();

	public User getUserById(Integer userId) throws ProjectException;

	public User updateUser(User user);

	public String deleteUserById(Integer userId) throws ProjectException;

}
