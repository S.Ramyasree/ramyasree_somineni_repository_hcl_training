import { Component, OnInit } from '@angular/core';
import { BookService } from '../_services/book.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  books:any;
  currentBook:any;
  currentIndex=-1;
  title='';
  constructor(private bookService:BookService) { }

  ngOnInit(): void {
    this.retrieveBooks();
  }
  retrieveBooks():void{
    this.bookService.getAll().subscribe(
      data=>{
        this.books=data;
        console.log(data);
      },
      error => {
        console.log(error);
      });
    
  }
  refreshList(): void {
    this.retrieveBooks();
    this.currentBook = null;
    this.currentIndex = -1;
  }

  setActiveBook(book:any, index:any): void {
    this.currentBook = book;
    this.currentIndex = index;
  }

  removeAllBook(): void {
    this.bookService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveBooks();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.bookService.findByTitle(this.title)
      .subscribe(
        data => {
          this.books = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
