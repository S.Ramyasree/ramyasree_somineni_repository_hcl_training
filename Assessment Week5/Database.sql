create database travel_ramyasree_somineni; 
use travel_ramyasree_somineni;
create table PASSENGER
(Passenger_name varchar(20), 

  Category               varchar(20),

   Gender                 varchar(20),

   Boarding_City      varchar(20),

   Destination_City   varchar(20),

  Distance                int,

  Bus_Type             varchar(20)

);
create table PRICE

(

             Bus_Type   varchar(20),

             Distance    int,

              Price      int

          );

 

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');

insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');

insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');

insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');

insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');

insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');

insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');

insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');

insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

 

select * from passenger;

 

insert into price values('Sleeper',350,770);

insert into price values('Sleeper',500,1100);

insert into price values('Sleeper',600,1320);

insert into price values('Sleeper',700,1540);

insert into price values('Sleeper',1000,2200);

insert into price values('Sleeper',1200,2640);

insert into price values('Sleeper',350,434);

insert into price values('Sitting',500,620);

insert into price values('Sitting',500,620);

insert into price values('Sitting',600,744);

insert into price values('Sitting',700,868);

insert into price values('Sitting',1000,1240);

insert into price values('Sitting',1200,1488);

insert into price values('Sitting',1500,1860);

 

select * from price;

 

SELECT 

max(Distance) as distance,

COUNT(CASE WHEN Gender='M' THEN 1  END) As Male,

COUNT(CASE WHEN Gender='F' THEN 1  END) As Female,

COUNT(*) as Total from passenger  where Distance>=600 GROUP BY Gender;

 

Select min(Price) as MinimumPrice from price where Bus_Type="Sleeper";

 

Select Passenger_name from passenger where Passenger_name LIKE "S%";

 

select p.passenger_name, p.Boarding_city, p.Destination_city, p.Bus_type, pi.price

 from passenger p inner join price pi

on p.distance=pi.distance and p.Bus_type=pi.Bus_type

group by p.passenger_name;

 

Select Passenger_name,price from passenger where Bus_type="Sitting" And Distance=1000 ;

 

select price.Bus_Type, price.price from price inner join passenger 

on passenger.Distance = price.Distance

where passenger.Passenger_name="Pallavi";

 

SELECT DISTINCT(Distance) FROM  passenger ORDER BY Distance desc;

 

select Distance,Passenger_name, round(Distance * 100.0 / (select sum(Distance) from passenger))

from passenger group by Passenger_name;

 

create view Query as select Passenger_name,Category from passenger where Category="AC"; 

select * from Query;

 

DELIMITER //

CREATE  PROCEDURE Passenger_list ()

BEGIN

select Passenger_name,Bus_Type

from passenger

where Bus_Type="Sleeper";

END //

DELIMITER ;

 Call Passenger_list;

 select * from passenger limit 5;