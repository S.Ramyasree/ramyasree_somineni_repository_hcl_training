create database ramyasree;

use ramyasree

create table moviescoming(
id int(5) primary key,
name varchar(20),type varchar(20),year int(4),rating int(2));

insert into moviescoming values(1,'Pushpa','action',2021,2);
insert into moviescoming values(2,'uppena','romantic',2018,3);
insert into moviescoming values(3,'Rang De','Drama',2016,5);
insert into moviescoming values(4,'vakeel saab','Thriller',2020,1);
insert into moviescoming values(5,'red','action',2019,4);

select * from moviescoming;
create table moviesintheater(
id int(5) primary key,
name varchar(20),type varchar(20),year int(4),rating int(2));

insert into moviesintheater values(1,'bhahubali','action',2017,4);
insert into moviesintheater values(2,'manam','Drama',2019,2);
insert into moviesintheater values(3,'pokiri','action',2020,5);
insert into moviesintheater values(4,'jersey','romantic',2018,3);
insert into moviesintheater values(5,'kanchana','action',2015,1);

select * from moviesintheater;
create table topratingmovies(
id int(5) primary key,
name varchar(20),type varchar(20),year int(4),rating int(2));

insert into topratingmovies values(1,'vedam','action',2010,5);
insert into topratingmovies values(2,'sankranthi','Drama',2012,1);
insert into topratingmovies values(3,'ArjunReddy','romantic',2014,2);
insert into topratingmovies values(4,'eega','Thriller',2015,3);
insert into topratingmovies values(5,'bommarillu','romantic',2013,4);

select * from topratingmovies;
create table topratingindian(
id int(5) primary key,
name varchar(20),type varchar(20),year int(4),rating int(2));

insert into topratingindian values(1,'raja','action',2016,4);
insert into topratingindian values(2,'goodachari','romantic',2020,2);
insert into topratingindian values(3,'mayabazar','Drama',2014,1);
insert into topratingindian values(4,'saaho','action',2013,3);
insert into topratingindian values(5,'seetha','romantic',2008,5);
select * from topratingindian;