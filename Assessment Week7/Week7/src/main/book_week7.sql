
create database bookstore;

use bookstore;

create table book(id int primary key,name varchar(100),author varchar(50),type varchar(30),image varchar(500));


insert into book values(1,"BUDDHA: Spirituality For Leadership & Success","Pranay","Spirituality","https://m.media-amazon.com/images/I/81damewvwxS._AC_UY327_FMwebp_QL65_.jpg");
insert into book values(2,"Travelogue Across Hemispheres","Bhupender Gupta","Travel Guide","https://m.media-amazon.com/images/I/71FQ5qET49L._AC_UY327_FMwebp_QL65_.jpg");
insert into book values(3,"Wild Weather National Geographic","R. Rowling","geographic","https://m.media-amazon.com/images/I/41W5H76a6ZS._AC_UY327_FMwebp_QL65_.jpg");
insert into book values(4,"Book Of Common Signs","Ashok Srinivasan","biography","https://m.media-amazon.com/images/I/712yz+57JDL._AC_UY327_FMwebp_QL65_.jpg");
insert into book values(5,"Rich Dad Poor Dad","Robert T. Kiyosaki","Autobiography","https://m.media-amazon.com/images/I/81dQwQlmAXL._AC_UY327_QL65_.jpg");




desc book;
select *from book;

create table login(emailId varchar(50) primary key,password varchar(20));

desc login;
select *from login;

create table registration(first_name varchar(30),last_name varchar(20),emailId varchar(40) primary key,password varchar(30),address varchar(80), contact int,gender varchar(10));

desc registration;
select *from registration;


    



