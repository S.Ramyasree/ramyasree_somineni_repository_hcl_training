<%@page import="com.greatlearning.assignment7.bean.Book"%>
<%@page import="com.greatlearning.assignment7.dao.BookDao"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ListIterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<tr><td><strong>User : <spam><%=session.getAttribute("login") %></spam>
</head>
<body bgcolor="lightblue">
<div style="text-align:center"><h3>Items added to readLater cart</h3></div>

<form action="ReadLaterController" method="post"></form>
<div style="text-align:center">
<div class="conatiner-fluid">
<!-- Book section -->
<table border="1">
	<tr>
			<th>S.No</th>
			<th>Book</th>
			<th>Id</th>
			<th>Name</th>
			<th>Author</th>
			
			
	</tr>

<%
   if(session.getAttribute("readLater")!=null){
	   ListIterator list=((List)session.getAttribute("readLater")).listIterator();
	   BookDao bd=new BookDao();
	   int a=0,total=0;
	   while(list.hasNext()){
		   a++;
	       int id=Integer.parseInt((String)list.next());
		   Book b=bd.getOne(id);
		   total++;
%>
		   <tr>
		        <td><%= a %></td>
		         <td><img src="<%=b.getImage()%>" width="150" height="150"></td>
		        <td><%= b.getId() %></td>
		        <td><%= b.getName() %></td>
		        <td><%= b.getAuthor() %></td>
		       
		   </tr>
		   <%
		       }
	       %>
	       <tr>
	       <td colspan="6"><strong>Total Books = <%= total%></strong></td>
	       <%
	          }
	       %>
	       </tr>
	       
	       <tr><td style="text-align:center" colspan="7"><button class="btn btn-success btn-lg"
	        onclick="location.href='Logout.jsp'">Logout</button>
	       </td></tr>
	   </div>
</div>
</table>

</body>
</html>