<%@page import="com.greatlearning.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h4 align="center">User Your ReadLater BookList is here</h4>
<a href="welcome.jsp">Click Here To Go Back To DashBoard</a>
<body style="background-color:yellow; ;">
<div align="center">
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>Image</th>
			
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME TO DASHBORAD Ms/Mr  "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"height="200px" width="150px"></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>
