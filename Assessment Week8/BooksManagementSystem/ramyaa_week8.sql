create database ramyaa_week8;
use ramyaa_week8;
create table Books (id int primary key,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512));
insert into Books values(1,"Do Epic Shit","Ankur","motivational","https://m.media-amazon.com/images/I/41VDwe3QOOL.jpg");

insert into Books values(2,"Pursuit of happiness","Michael","Novel","https://miro.medium.com/max/548/1*Y8vXN1mJeEHyXWJtFICjiQ.jpeg");

insert into Books values(3,"Happiness","Kathey","fantasy","https://image.shutterstock.com/image-vector/international-day-happiness-design-template-260nw-1551422423.jpg");

insert into Books values(4,"KingFisher","Vijay Malley","Good Times","https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Kingfisher_Airlines_Airbus_A320-200.jpg/800px-Kingfisher_Airlines_Airbus_A320-200.jpg");

insert into Books values(5,"AFCAT","Twin Brothers","Educational","https://www.exams88.in/wp-content/uploads/2018/12/AFCAT-2021.jpg");

insert into Books values(6,"Journey","A.P.J._Abdul_Kalam","Classics","https://images-na.ssl-images-amazon.com/images/I/41+h0fYL-eL._SY344_BO1,204,203,200_.jpg");

insert into Books values(7,"Ignited_minds","A.P.J._Abdul_Kalam","NonFiction","https://images-eu.ssl-images-amazon.com/images/I/41uV5nrOfsL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");

insert into Books values(8,"Wings_of_fire","A.P.J._Abdul_Kalam","AutoBiography","https://images-eu.ssl-images-amazon.com/images/I/51vgy3LMz6L._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");

insert into Books values(9,"Swami_Vivekananda","Swami_Vivekananda","Biography","https://images-eu.ssl-images-amazon.com/images/I/41raS4oJssL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");

insert into Books values(10,"A_biography","Mamta_Sharma","Biography","https://images-eu.ssl-images-amazon.com/images/I/51ac9W8HqCL._SX198_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(11,"Alexander_the_Great","Jacob_Abbott","Biography","https://images-na.ssl-images-amazon.com/images/I/51DVQNCm0+S._SX258_BO1,204,203,200_.jpg");
 create table Login(email varchar(20) primary key,password varchar(20) not null);
 create table LikedBooks(id int,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512),user varchar(20));
 
create table readlaterbooks(id int,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512),user varchar(20));

