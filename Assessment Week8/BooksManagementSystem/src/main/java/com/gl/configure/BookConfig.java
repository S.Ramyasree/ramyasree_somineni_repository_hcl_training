package com.gl.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.gl.beans.Books;

@Configuration
public class BookConfig {
	
	@Bean
	@Scope(value = "prototype")
	public Books books() {

		return new Books();
	}
	
	@Bean
	public DriverManagerDataSource ds() {

		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl("jdbc:mysql://localhost:3306/springmvc");
		ds.setUsername("root");
		ds.setPassword("Ramya@42");

		return ds;

	}

	@Bean
	public JdbcTemplate template() {

		JdbcTemplate template = new JdbcTemplate();
		template.setDataSource(ds());
		return template;

	}
	
	

}
