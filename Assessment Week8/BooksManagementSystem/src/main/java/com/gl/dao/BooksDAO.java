package com.gl.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gl.beans.Books;

public class BooksDAO {

	@Autowired
	private JdbcTemplate template;

	public List<Books> getAllBooks() {
		String sql = "select * from books";
		return template.query(sql, new BooksRowMapper());
	}
	
	public String addLikeSection(Books books) {
		String sql = "insert into like_section(id,name,genre) values(" + books.getId() + ", '" + books.getName() + "', '" + books.getGenre() + "')";
		template.update(sql);
		return "Added to Like";
	}
	
	public List<Books> getAllBooksFromLikeSection() {
		String sql = "select * from like_section";
		return template.query(sql, new BooksRowMapper());
	}
	
	public String addRedLater(Books books) {
		String sql = "insert into read_later(id,name,genre) values(" + books.getId() + ", '" + books.getName() + "', '" + books.getGenre() + "')";
		template.update(sql);
		return "Added to ReadLater";
	}
	
	public List<Books> getAllBooksFromReadLater() {
		String sql = "select * from read_later";
		return template.query(sql, new BooksRowMapper());
	}

}
