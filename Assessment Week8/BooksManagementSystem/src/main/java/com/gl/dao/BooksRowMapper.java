package com.gl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gl.beans.Books;

public class BooksRowMapper implements RowMapper<Books> {

	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub

		Books b = new Books();
		b.setId(rs.getInt(1));
		b.setName(rs.getString(2));
		b.setGenre(rs.getString(3));
		return b;
	}

}
