<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="menu.jsp"></jsp:include>
	<center>
		<h3>Login</h3>
		<form action="login" method="post">
			Username: <input type="text" name="username" /> <br> <br>
			Password: <input type="password" name="password" /><br> <br>
			<input type="submit" value="Login">
		</form>
		<br /> <br /> Not Registered.? <a href="register.jsp" style="font-weight:bold;">Register
			here</a> <br /> <br />
	</center>
</body>
</html>