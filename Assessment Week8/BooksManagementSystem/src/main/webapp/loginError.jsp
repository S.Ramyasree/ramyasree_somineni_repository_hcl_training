<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Error</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="menu.jsp"></jsp:include>

	<br>
	<br>

	<center>
		Invalid Credentials <br /> <br />Login with Correct credentials --->
		<a href="login.jsp" style="font-weight: bold;">Login</a> <br /> <br />
		Not registered...? Register here ---> <a href="register.jsp"
			style="font-weight: bold;">Register</a>
	</center>

</body>
</html>