<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Read Later</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="menu.jsp"></jsp:include>

	<center><h2>Read Later Section</h2></center>

	<%
	String username = request.getParameter("username");
	out.print("<center><h4>Welcome " + username + "</h4></center<br/ >");
	%>
	<table border="1" width="auto" align="center">

		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Genre</b></th>
		</tr>

		<c:forEach var="i" items="${books}">
			<tr>
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getGenre() }"></c:out></td>
			</tr>
		</c:forEach>

	</table>

</body>
</html>