<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="menu.jsp"></jsp:include>

	<center>
		<h3>Register</h3>
		<form action="register" method="post">
			Username: <input type="text" name="username" /> <br> <br>
			Password: <input type="password" name="password" /><br> <br>
			<input type="submit" value="Register">
		</form>
		<br /> <br /> Already Registered.? <a href="login.jsp" style="font-weight:bold;">Login
			here</a> <br /> <br />
	</center>
</body>
</html>