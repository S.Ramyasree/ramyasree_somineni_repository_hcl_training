<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success Login</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="menu.jsp"></jsp:include>

	<%
	String username = request.getParameter("username");
	out.print("<center><h2>Welcome " + username + "..! </h2></center<br/ >");
	request.getSession().setAttribute("username", username);
	%>

	<div align=right>
		<input type='button' value='logout'
			onclick="location.href= 'login.jsp'">
	</div>

	<h3>Displaying all available books</h3>
	<br />
	<br />

	<table border="1" width="auto" align="center" >
		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Genre</b></th>
			<th colspan=2><b>Action</b></th>
		</tr>
		
		<c:forEach var="i" items="${books}">
			<tr>
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getGenre() }"></c:out></td>
				<td><a href="readLater?action=add&bookId=<c:out value="${i.getId()}"/>&bookName=<c:out value="${i.getName()}"/>&bookGenre=<c:out value="${i.getGenre()}"/>">Read Later</a></td>
				<td><a href="like?action=add&bookId=<c:out value="${i.getId()}"/>&bookName=<c:out value="${i.getName()}"/>&bookGenre=<c:out value="${i.getGenre()}"/>">Like</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>