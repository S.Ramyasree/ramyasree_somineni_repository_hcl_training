package com.greatlearning.bookiesspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookiesSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookiesSpringBootApplication.class, args);
	}

}
