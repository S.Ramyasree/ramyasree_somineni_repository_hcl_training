package com.hcl.gradedassignment.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.Data;

import javax.persistence.*;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;


@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long userId;
    @Column
    private String username;
    @Column
    @JsonIgnore
    private String password;
 

    @ManyToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
    		name = "user_roles", 
    		joinColumns = { @JoinColumn(name = "user_id") }, 
    		inverseJoinColumns = { @JoinColumn(name = "role_id") }
    )
    private List<Role> roles;
    
    @ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "user_likes",
		joinColumns = { @JoinColumn(name = "user_id") },
		inverseJoinColumns = { @JoinColumn (name = "liked_id")}
	)
	private List<LikedBook> likedBooks;

	public void setUsername(String username2) {
		// TODO Auto-generated method stub
		
	}

	public void setPassword(String encode) {
		// TODO Auto-generated method stub
		
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<SimpleGrantedAuthority> getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setRoles(List<Role> roleList) {
		// TODO Auto-generated method stub
		
	}

	public void setLikedBooks(List<LikedBook> likelist) {
		// TODO Auto-generated method stub
		
	}

	public List<LikedBook> getLikedBooks() {
		// TODO Auto-generated method stub
		return null;
	}

    
}
