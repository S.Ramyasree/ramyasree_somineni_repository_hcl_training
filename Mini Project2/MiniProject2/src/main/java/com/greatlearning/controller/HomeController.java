package com.greatlearning.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.greatlearning.service.CustomersService;
import com.greatlearning.users.Customers;

@Controller
public class HomeController {

	@Autowired
	private CustomersService customerService;

	@GetMapping("/")
	public String cust(Model m) {
		List<Customers> customer=customerService.getAllCustomer();
		m.addAttribute("customer",customer);
		return "index";
	}

	@GetMapping("/add")
	public String addCust() {
		return "add";
	}

	@PostMapping("/register")
	public String customersRegistration(@ModelAttribute Customers e,HttpSession session) {
		System.out.println(e);
		customerService.addCustomer(e);
		session.setAttribute("msg", "Customer Data Added");
		return "redirect:/";
	}

	@GetMapping("/edit/{id}")
	public String editcust(@PathVariable int id,Model m) {
		Customers customer=customerService.getCustomerById(id);
		m.addAttribute("customer", customer);
		return "edit";
	}

	@PostMapping("/update")
	public String updateCust(@ModelAttribute Customers customer,HttpSession session) {
		customerService.addCustomer(customer);
		System.out.println(customer);
		session.setAttribute("msg", "Employee Data Updated");
		return "redirect:/";

	}

	@GetMapping("/delete/{id}")
	public String deleteCust(@PathVariable int id,HttpSession session) {
		System.out.println("Deleted Id = "+id );
		customerService.deleteCust(id);
		session.setAttribute("msg", "Customer Data Deleted");
		return "redirect:/";
	}

}
