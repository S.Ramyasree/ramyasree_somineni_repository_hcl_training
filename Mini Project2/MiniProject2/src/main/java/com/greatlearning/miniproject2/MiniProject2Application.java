package com.greatlearning.miniproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.greatlearning.miniproject2","com.greatlearning.controller","com.greatlearning.service","com.greatlearning.repository","com.greatlearning.users"})
public class MiniProject2Application {

	public static void main(String[] args) {
		SpringApplication.run(MiniProject2Application.class, args);
	}

}
