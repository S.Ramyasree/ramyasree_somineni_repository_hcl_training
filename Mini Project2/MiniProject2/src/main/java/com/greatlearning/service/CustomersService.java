package com.greatlearning.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.repository.CustomersRepository;
import com.greatlearning.users.Customers;

@Service	
public class CustomersService {

	@Autowired
	public CustomersRepository customersRepository;

	public void addCustomer(Customers customer) {
		customersRepository.save(customer);
	}

	public List<Customers> getAllCustomer(){
		return customersRepository.findAll();
	}

	public Customers getCustomerById(int id) {
		Optional<Customers> customer=customersRepository.findById(id); 
		if(customer.isPresent()) {
			return customer.get();
		}
		return null;
	}

	public void deleteCust(int id) {
		System.out.println("Deleted Cust Id = "+id);
		customersRepository.deleteById(id);
	}
}
