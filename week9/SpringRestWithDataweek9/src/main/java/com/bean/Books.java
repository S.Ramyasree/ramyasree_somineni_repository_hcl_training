package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter					// all setter methods. 
@Getter					// all getter methods 
@ToString				// toString() method 
@NoArgsConstructor			// empty constructor 
@RequiredArgsConstructor			// parameterized constructor 
public class Books{
@Id
private int bid;
private String bname;
private float price;
public int getBid() {
	return bid;
}
public void setBid(int bid) {
	this.bid = bid;
}
public String getBname() {
	return bname;
}
public void setBname(String bname) {
	this.bname = bname;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}

}
