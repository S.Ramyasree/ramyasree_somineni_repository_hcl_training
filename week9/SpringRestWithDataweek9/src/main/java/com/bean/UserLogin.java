package com.bean;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Table(name = "UserLogin")
public class UserLogin {
	private @Id  @GeneratedValue int id;
    private @NotBlank String username;
    private @NotBlank String password;
    private @NotBlank boolean logged_In;

    public UserLogin() {
    }

    public UserLogin(@NotBlank String username, 
                @NotBlank String password) {
        this.username = username;
        this.password = password;
        this.logged_In = false;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogged_In() {
        return logged_In;
    }

    public void setLogged_In(boolean logged_In) {
        this.logged_In = logged_In;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLogin)) return false;
        UserLogin user = (UserLogin) o;
        return Objects.equals(username, user.username) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, 
                            logged_In);
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", logged_In=" + logged_In +
                '}';
    }
}

