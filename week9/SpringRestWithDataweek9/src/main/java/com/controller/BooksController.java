package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Books;
import com.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {

	@Autowired
	BooksService booksService;
	
	@GetMapping(value = "getAllBooks",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllBooksInfo() {
		return booksService.getAllBooks();
	}
	@PostMapping(value = "storeBooks",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooksInfo(@RequestBody Books book) {
		
				return booksService.storeBooksInfo(book);
	}
	@DeleteMapping(value = "deleteBooks/{bid}")
	public String storeBooksInfo(@PathVariable("bid") int bid) {
					return booksService.deleteBooksInfo(bid);
	}
	
	@PatchMapping(value = "updateBooks")
	public String updateBooksInfo(@RequestBody Books book) {
					return booksService.updateBooksInfo(book);
	}


}



